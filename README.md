# Basenji saturation scores Shiny


## Description

This Shiny app allows for simple and interactive visualization of Besenji saturation scores predictions.
It presents and interactive table to subset the 175 predictions and simple visualization by clicking on the row of interest. It provides a heatmap with saturation scores, a line plot with the deltas and the corresponding motifs based on importance scores and divide by positive and negative effects on signal. The regions that can be visualized span from ±25bp from the causal variant to ±75bp.

## Runnning the app

To run the app use the table provided in the /data folder. You can then run the script 'app.R' and it will lanch a shiny istance.


