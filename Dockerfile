# Install R
FROM r-base:4.2.2

# Set maintainer/author label
# Overrides label from base image
LABEL org.opencontainers.image.authors="MattiaForneris (mattia.forneris@embl.de)"
LABEL org.opencontainers.image.source="https://git.embl.de/forneris/basenji-saturation-scores-shiny"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.vendor=""

#RUN apt update && apt install -y libssl-dev liblzma-dev libbz2-dev libicu-dev libtiff-dev libfftw3-dev libcurl4-openssl-dev libxml2-dev libssh2-1-dev libgit2-dev libfontconfig1-dev libfreetype6-dev libharfbuzz-dev libfribidi-dev
RUN apt update
RUN apt install -y cmake libssl-dev libcurl4-openssl-dev libxml2-dev libssh2-1-dev libgit2-dev libgsl-dev


# Install required R packages
RUN R -e "install.packages(c('shiny', 'DT', 'dplyr', 'ggplot2', 'ggpubr', 'gridExtra', 'RColorBrewer', 'pheatmap', 'stringr', 'ggrepel', 'BiocManager', 'grImport2'), repos=c('https://cloud.r-project.org/', 'https://ftp.gwdg.de/pub/misc/cran/', 'http://www.bioconductor.org'))"
RUN R -e "BiocManager::install(ask = F)" && R -e "BiocManager::install(c('motifStack'))"

# Copy the app to the image
RUN mkdir -p /usr/local/app/basenji-saturation-scores-shiny
COPY app.R /usr/local/app/basenji-saturation-scores-shiny
COPY data /usr/local/app/basenji-saturation-scores-shiny/data

# init data for app, this is producing the app_input.rds in input
WORKDIR /usr/local/app/basenji-saturation-scores-shiny
WORKDIR /

# IDE is available on port 9081
EXPOSE 9081

# Create a directory to mount eventual data
RUN mkdir -p /analysis
ENV DATADIR="/analysis"

# Create user
RUN useradd -m bob -d /home/bob

# Run as user ide to avoid running as root or require user to run
# with the --user flag
USER bob

CMD ["R", "-e", "shiny::runApp('/usr/local/app/basenji-saturation-scores-shiny/app.R', host = '0.0.0.0', port = 9081)"]



